package com.example.api001.Controller;

import com.example.api001.Entity.Customer;
import com.example.api001.ResponseEntity.CustomerResponse;
import com.example.api001.Entity.CustomerWithoutID;
import com.example.api001.ResponseEntity.ValidateWithoutList;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.ArrayList;

@RestController
public class CustomerController {

    ArrayList<Customer> customers = new ArrayList<>();
    public CustomerController(){
        customers.add(new Customer("Narith","male",20,"pp"));
        customers.add(new Customer("Neary","female",21,"pvh"));
        customers.add(new Customer("Naroth","male",21,"sr"));
    }

    //get All customers
    @GetMapping("/api/v1/customers")
    public ResponseEntity<?> getAllCustomer(){
        if(!(customers.isEmpty())){
            return ResponseEntity.ok(new CustomerResponse<ArrayList<Customer>>(
                    "Success",
                    customers,
                    LocalDateTime.now(),
                    "ok"
            ));
        }
  return ResponseEntity.ok("Not data in list");
    }

    // Insert into ArrayList
    @PostMapping("/api/v1/customer")
    public ResponseEntity<?> insertProduct(@RequestBody CustomerWithoutID customerWithoutID ){
        String validateInputString= "[a-zA-Z]+";
        boolean found = false;

        Customer customer = new Customer(customerWithoutID.getName() ,customerWithoutID.getGender(), customerWithoutID.getAge(),customerWithoutID.getAddress());
        customers.add(customer);

        if(customerWithoutID.getGender().equals("string")){
            return ResponseEntity.ok(new ValidateWithoutList(
                    "Fill Gender only male,female, f and m",
                    LocalDateTime.now(),
                    "Not okay"
            ));
        } else if (customerWithoutID.getName().isEmpty()||customerWithoutID.getGender().isEmpty()||customerWithoutID.getAddress().isEmpty()||(customerWithoutID.getAge()<=0)) {
            return ResponseEntity.ofNullable(new ValidateWithoutList(
                    "Is Empty",
                    LocalDateTime.now(),
                    "Not okay"
            ));
        } else if (customerWithoutID.getName().matches(validateInputString)&&customerWithoutID.getGender().matches(validateInputString)&&customerWithoutID.getAddress().matches(validateInputString)){
            return ResponseEntity.ofNullable(new CustomerResponse<ArrayList<Customer>>(
                    "Success",
                    customers,
                    LocalDateTime.now(),
                    "ok"
            ));
        } else if (found ==false){
            return ResponseEntity.badRequest().body(new ValidateWithoutList(
                    " Valid Data",
                    LocalDateTime.now(),
                    "Not Valid Data"
            ));
        }

        return null;
    }

    //Search Customer by id
    @GetMapping("/api/v1/customers/{customerId}")
    public ResponseEntity<?> getProductById(@PathVariable("customerId") Integer proId){
        for(Customer pro : customers){
            if(pro.getId() == proId){
                return ResponseEntity.ok(new CustomerResponse<Customer>(
                        "This record has found successfully",
                        pro,
                        LocalDateTime.now(),
                        "Ok"
                ));
            }
        }
        return ResponseEntity.ofNullable(new ValidateWithoutList(
                "ID not found!!",
                LocalDateTime.now(),
                "Not okay"
        ));
    }

    //Search Customer by name
    @GetMapping("/api/v1/customers/search")
    public ResponseEntity<?> getCustomerByName(@RequestParam String cusName){
        for(Customer cus : customers){
            if(cus.getName().equals(cusName)){
                return ResponseEntity.ok(new CustomerResponse<Customer>(
                        "This record has found successfully",
                         cus,
                        LocalDateTime.now(),
                        "Ok"
                ));
            }
        }
        return ResponseEntity.ofNullable(new ValidateWithoutList(
                "Name Not Found!!",
                LocalDateTime.now(),
                "Not Okay"
        ));
    }


    //Update Customer by id
    @PutMapping("/api/v1/customers/{customerId}")
    public ResponseEntity<?> updateCustomerByID(@RequestBody CustomerWithoutID customerWithoutID,@PathVariable("customerId") Long idUpdate ){
        for(Customer cus :customers){
            if (cus.getId() == idUpdate ){
                cus.setName(customerWithoutID.getName());
                cus.setGender(customerWithoutID.getGender());
                cus.setAge(customerWithoutID.getAge());
                cus.setAddress(customerWithoutID.getAddress());

              return ResponseEntity.ok(new CustomerResponse<Customer>(
                      "This record updated successfully",
                      cus,
                      LocalDateTime.now(),
                      "ok"
              ));
            }
        }
        return ResponseEntity.ofNullable(new ValidateWithoutList(
                "Update Not success",
                LocalDateTime.now(),
                "Not okay"
        ));
    }


    //Delete Customer by id
    @DeleteMapping("/api/v1/customers/{customerId}")
    public ResponseEntity<?> deleteCustomerByID(@PathVariable("customerId") int idDelete){
        for(Customer cus : customers){
           if (cus.getId() == idDelete){
               customers.remove(cus);
               return ResponseEntity.ok(new ValidateWithoutList(
                       "You delete successfully",
                       LocalDateTime.now(),
                       "ok"

               ));
           }

        }
        return ResponseEntity.ofNullable(new ValidateWithoutList(
                "You input wrong id to delete",
                LocalDateTime.now(),
                "Input Again!!!"

        ));
    }
}
